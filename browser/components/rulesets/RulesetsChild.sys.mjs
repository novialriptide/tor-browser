// Copyright (c) 2022, The Tor Project, Inc.

"use strict";

import { RemotePageChild } from "resource://gre/actors/RemotePageChild.sys.mjs";

export class RulesetsChild extends RemotePageChild {}
